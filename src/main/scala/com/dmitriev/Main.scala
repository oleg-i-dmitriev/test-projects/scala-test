package com.dmitriev

import scala.io.Source.fromFile
import scala.util.Using

object Main {
  val borderValue = 10

  def main(args: Array[String]): Unit = {
    val squares = read("src/main/resources/input.txt")
    findValidPermutation(squares.length, squares)
  }

  def read(path: String): Array[Square] = {
    val squares = new Array[Square](12)

    Using(fromFile(path)) { reader =>
      val lines = reader.getLines()

      for (i <- 0 until 12) {
        val numbers = lines.next().split(" ")
        squares(i) = Square(numbers(0).toInt, numbers(1).toInt, numbers(2).toInt, numbers(3).toInt)
      }
    }

    squares
  }

  def findValidPermutation(n: Int, squares: Array[Square]): Unit = {
    if (n == 1) {
      if (check(squares)) print(squares)
    }
    else {
      for (i <- 0 until n - 1) {
        findValidPermutation(n - 1, squares)
        val swapIndex = if (n % 2 == 0) i else 0
        swap(squares, swapIndex, n - 1)
      }

      findValidPermutation(n - 1, squares)
    }
  }

  def print(squares: Array[Square]): Unit = {
    squares.foreach(s => println(Array(s.topLeft, s.topRight, s.bottomLeft, s.bottomRight).mkString(" ")))
    println()
  }

  def swap(squares: Array[Square], a: Int, b: Int): Unit = {
    val tmp = squares(a)
    squares(a) = squares(b)
    squares(b) = tmp
  }

  def check(squares: Array[Square]): Boolean = {
    // four squares condition
    if (squares(0).bottomRight + squares(1).bottomLeft + squares(3).topRight + squares(4).topLeft != borderValue) return false
    if (squares(2).bottomRight + squares(3).bottomLeft + squares(6).topRight + squares(7).topLeft != borderValue) return false
    if (squares(3).bottomRight + squares(4).bottomLeft + squares(7).topRight + squares(8).topLeft != borderValue) return false
    if (squares(4).bottomRight + squares(5).bottomLeft + squares(8).topRight + squares(9).topLeft != borderValue) return false
    if (squares(7).bottomRight + squares(8).bottomLeft + squares(10).topRight + squares(11).topLeft != borderValue) return false

    // three squares condition
    if (squares(0).bottomLeft + squares(2).topRight + squares(3).topLeft > borderValue) return false
    if (squares(1).bottomRight + squares(4).topRight + squares(5).topLeft > borderValue) return false
    if (squares(6).bottomRight + squares(7).bottomLeft + squares(10).topLeft > borderValue) return false
    if (squares(8).bottomRight + squares(9).bottomLeft + squares(11).topRight > borderValue) return false

    // two squares condition
    if (squares(0).topRight + squares(1).topLeft > borderValue) return false
    if (squares(2).bottomLeft + squares(6).topLeft > borderValue) return false
    if (squares(5).bottomRight + squares(9).topRight > borderValue) return false
    if (squares(10).bottomRight + squares(11).bottomLeft > borderValue) return false

    true
  }
}
